﻿namespace EventPlanner.Application.Common.Interfaces;

public interface IDateTime
{
    DateTime Now { get; }
}
