﻿using EventPlanner.Application.TodoLists.Queries.ExportTodos;

namespace EventPlanner.Application.Common.Interfaces;

public interface ICsvFileBuilder
{
    byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
}
