﻿using EventPlanner.Application.Common.Mappings;
using EventPlanner.Domain.Entities;

namespace EventPlanner.Application.TodoLists.Queries.ExportTodos;

public class TodoItemRecord : IMapFrom<TodoItem>
{
    public string? Title { get; set; }

    public bool Done { get; set; }
}
