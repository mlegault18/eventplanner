﻿using MediatR;

namespace EventPlanner.Domain.Common;

public abstract class BaseEvent : INotification
{
}
