﻿using Microsoft.AspNetCore.Identity;

namespace EventPlanner.Infrastructure.Identity;

public class ApplicationUser : IdentityUser
{
}
