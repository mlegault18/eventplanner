﻿using EventPlanner.Application.Common.Interfaces;

namespace EventPlanner.Infrastructure.Services;

public class DateTimeService : IDateTime
{
    public DateTime Now => DateTime.Now;
}
